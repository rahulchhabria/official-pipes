pyyaml==3.13
cerberus==1.2
requests==2.21.0
docker==3.7.0
joblib==0.13.2
colorlog==4.0.2
GitPython==3.0.8
